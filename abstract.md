In 2019, Paraskevopoulou and Appel demonstrated that the program
transformation called "closure conversion" is safe for space, in the
sense that any increase in space usage due to the program
transformation is bounded by a constant factor that depends on the
program source, but not on its inputs. This is an important result as
few guarantees on space usage exist in the literature.

Unfortunately, the formal result proved in this previous work is both
very specific and fairly complex. Specific because it fits inside the
development of a verified compiler, so it is expressed on
a less-standard lambda-calculus in CPS form, with explicit reasoning
about garbage collection and a cost model defined through
a non-trivial big-step operational semantics. Complex because the
proof uses step-indexed logical relation, with an extra "fuel"
technique to get divergence-preservation results. Reusing this
approach to prove similar space-preservation results for other
lambda-calculi seems daunting.

In the present work, we prove a space-preservation result for an
almost-standard call-by-value lambda-calculus -- almost-standard as it
uses an explicit heap to give an easy definition of space usage --
using an elementary proof technique, namely a simulation argument. We
believe that this simpler setting and simpler proof technique make the
approach easier to reuse to prove space safety in other settings.