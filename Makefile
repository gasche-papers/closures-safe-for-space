LATEXMK=latexmk -pdf -bibtex
TARGETS=closure-safe-for-space.pdf

all: $(TARGETS)
.PHONY: $(TARGETS)
closure-safe-for-space.pdf:
	$(LATEXMK) closure-safe-for-space.tex

clean:
	$(LATEXMK) -c
